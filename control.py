from typing import List

from nmigen import (
    Array, Cat, ClockDomain, Const, Elaboratable, Module, Record, Signal,
)
from nmigen.back.pysim import Simulator
from nmigen.build import Platform
from nmigen.cli import main_parser

from address_counter import AddressCounter
from alu import ALU
from common import *
from conditions import ConditionShaper
from decoder import Decoder, InstrMode
from instr_set import *
from latch import Latch
from registers import RegisterFile


class ControlUnit(Elaboratable):

    def __init__(self):

        # bus interface
        self.addr_out = Signal(15)  # TODO: implement MCU
        self.data_out = Signal(16)
        self.req_out = Signal()  # bus requested
        self.we_lo_out = Signal()  # bus write (low byte)
        self.we_hi_out = Signal()  # bus write (high byte)
        self.data_in = Signal(16)
        self.grant_in = Signal()  # bus granted

        # internal interface and storage
        self.addr_0 = Signal()
        self.constants = Array(
            [Const(0, 16), Const(1, 16), Const(2, 16), Const(4, 16)]
        )
        self.stage = Signal(Stage)
        self.instr_size = Signal(ConstAddr)  # size of the previous instruction
        self.msw = Record([
            ('c', 1),  # carry/borrow
            ('v', 1),  # arithmetic overflow
            ('z', 1),  # zero
            ('n', 1),  # negative
            ('p', 1),  # parity
        ])  # flags and other status indicators

        self.debug_out = Signal(16)

    def elaborate(self, platform: Platform) -> Module:

        def save_all_flags():
            m.d.sync += (
                self.msw.c.eq(alu.c_out),
                self.msw.v.eq(alu.v_out),
                self.msw.z.eq(alu.z_out),
                self.msw.n.eq(alu.n_out),
                self.msw.p.eq(alu.p_out),
            )

        def save_logic_flags():
            m.d.sync += (
                self.msw.z.eq(alu.z_out),
                self.msw.p.eq(alu.p_out),
            )

        def do_normal_dyadic(op_type: InstructionType):
            with m.Case(op_type.op('AND')):
                m.d.comb += (
                    alu.op_in.eq(ALUOp.AND),
                    registers.wb_data_in.eq(alu.s_out),
                )
                save_logic_flags()

            with m.Case(op_type.op('OR')):
                m.d.comb += (
                    alu.op_in.eq(ALUOp.OR),
                registers.wb_data_in.eq(alu.s_out),
                )
                save_logic_flags()

            with m.Case(op_type.op('XOR')):
                m.d.comb += (
                    alu.op_in.eq(ALUOp.XOR),
                    registers.wb_data_in.eq(alu.s_out),
                )
                save_logic_flags()

            with m.Case(op_type.op('BRS')):
                m.d.comb += (
                    alu.op_in.eq(ALUOp.AND),
                    alu.invert_a_in.eq(1),
                    registers.wb_data_in.eq(alu.s_out),
                )
                save_logic_flags()

            with m.Case(op_type.op('ADD')):
                m.d.comb += (
                    alu.op_in.eq(ALUOp.ADD),
                    alu.c_ctrl_in.eq(ALUCCtrl.ZERO),
                    registers.wb_data_in.eq(alu.s_out),
                )
                save_all_flags()

            with m.Case(op_type.op('ADC')):
                m.d.comb += (
                    alu.op_in.eq(ALUOp.ADD),
                    alu.c_ctrl_in.eq(ALUCCtrl.ZERO),
                    registers.wb_data_in.eq(alu.s_out),
                )
                save_all_flags()

            with m.Case(op_type.op('SUB')):
                m.d.comb += (
                    alu.op_in.eq(ALUOp.ADD),
                    alu.c_ctrl_in.eq(ALUCCtrl.ONE),
                    alu.invert_a_in.eq(1),
                    registers.wb_data_in.eq(alu.s_out),
                )
                save_all_flags()

            with m.Case(op_type.op('SBB')):
                m.d.comb += (
                    alu.op_in.eq(ALUOp.ADD),
                    alu.c_ctrl_in.eq(ALUCCtrl.INVERT),
                    alu.invert_a_in.eq(1),
                    registers.wb_data_in.eq(alu.s_out),
                )
                save_all_flags()

            with m.Case(op_type.op('CMP')):
                m.d.comb += (
                    alu.op_in.eq(ALUOp.ADD),
                    alu.c_ctrl_in.eq(ALUCCtrl.ONE),
                    alu.invert_a_in.eq(1),
                    registers.wb_data_in.eq(alu.s_out),
                    registers.wb_enable_in.eq(0),
                )
                save_all_flags()

            with m.Case(op_type.op('LOAD')):
                m.d.comb += (
                    alu.op_in.eq(ALUOp.OR),
                    alu.b_in.eq(self.constants[ConstAddr.ZERO]),
                    registers.wb_data_in.eq(alu.s_out),
                )

        m = Module()

        m.submodules.offset = offset = Latch(16)  # offset
        m.submodules.data_lo = data_lo = Latch(8)
        m.submodules.data_hi = data_hi = Latch(8)

        m.submodules.alu = alu = ALU()
        m.submodules.registers = registers = RegisterFile()
        m.submodules.decoder = decoder = Decoder()
        m.submodules.addr_cnt = addr_cnt = AddressCounter()
        m.submodules.cond = cond = ConditionShaper()

        m.d.comb += (
            cond.c_in.eq(self.msw.c),
            cond.v_in.eq(self.msw.v),
            cond.z_in.eq(self.msw.z),
            cond.n_in.eq(self.msw.n),
            cond.p_in.eq(self.msw.p),
        )

        # wait for offset
        m.d.comb += offset.data_in.eq(self.data_in)

        m.d.comb += (
            self.addr_out.eq(addr_cnt.addr_out[1:]),
            self.addr_0.eq(addr_cnt.addr_out[0]),
        )

        with m.Switch(self.stage):
            with m.Case(Stage.INSTR_REQ, Stage.NEXT_INSTR_REQ):

                # increment IP
                m.d.comb += (
                    registers.ra_addr_in.eq(Reg.IP),
                    alu.a_in.eq(registers.ra_data_out),
                    alu.b_in.eq(self.constants[self.instr_size]),
                    alu.op_in.eq(ALUOp.ADD),
                    alu.c_ctrl_in.eq(ALUCCtrl.ZERO),
                )
                # write IP
                m.d.comb += (
                    registers.wa_addr_in.eq(Reg.IP),
                    registers.wa_data_in.eq(alu.s_out),
                    registers.wa_enable_in.eq(1),
                )
                # save new IP in temporary latches, because our register
                # file's read ports may be both busy on the next stage
                m.d.comb += (
                    data_lo.data_in.eq(alu.s_out[:8]),
                    data_lo.en_in.eq(1),
                    data_hi.data_in.eq(alu.s_out[8:]),
                    data_hi.en_in.eq(1),
                )
                # output temporary latches to the address bus
                m.d.comb += (
                    self.addr_out.eq(
                        Cat(data_lo.data_out[1:], data_hi.data_out)
                    ),
                    self.addr_0.eq(data_lo.data_out[0]),
                )
                m.d.sync += self.req_out.eq(1)

                with m.If(decoder.mode_out.matches(
                    InstrMode.PREINCREMENT, InstrMode.POSTDECREMENT,
                )):
                    # get memory addressing register
                    mem_reg = Signal(4)

                    with m.If(decoder.instr_out.matches(DI.mask)):
                        m.d.comb += mem_reg.eq(decoder.reg_offhand_out)
                    with m.Else():
                        m.d.comb += mem_reg.eq(decoder.reg_main_out)

                    # read from/write to B channel of register file
                    m.d.comb += (
                        registers.rb_addr_in.eq(mem_reg),
                        registers.wb_addr_in.eq(mem_reg),
                        registers.wb_byte_in.eq(0),
                        registers.wb_enable_in.eq(1),
                        addr_cnt.addr_in.eq(registers.rb_data_out),
                        addr_cnt.count_in.eq(1),
                        addr_cnt.en_in.eq(1),
                    )
                    with m.If(mem_reg == Reg.IP):
                        m.d.comb += addr_cnt.byte_in.eq(0)
                    with m.Else():
                        m.d.comb += addr_cnt.byte_in.eq(decoder.b_out)

                    with m.If(InstrMode.POSTDECREMENT):
                        m.d.comb += addr_cnt.down_in.eq(1)

                    m.d.comb += registers.wb_data_in.eq(addr_cnt.addr_out)

                # reset previous instruction size
                m.d.sync += self.instr_size.eq(ConstAddr.ZERO)

                # advance to next stage
                with m.If(
                    (self.stage == Stage.NEXT_INSTR_REQ)
                    & (cond.result_out == 1)
                ):
                    m.d.sync += self.stage.eq(Stage.NEXT_INSTR_RSP)
                with m.Else():
                    m.d.sync += self.stage.eq(Stage.INSTR_RSP)

            with m.Case(Stage.NEXT_INSTR_RSP):
                with m.If(self.grant_in == 1):
                    # free bus
                    m.d.sync += self.req_out.eq(0)

                    m.d.sync += self.instr_size.eq(decoder.skip_size)
                    m.d.sync += self.stage.eq(Stage.INSTR_REQ)

            with m.Case(Stage.INSTR_RSP):

                # hold the address
                m.d.comb += self.addr_out.eq(
                    Cat(data_lo.data_out[1:], data_hi.data_out)
                )
                m.d.comb += self.addr_0.eq(data_lo.data_out[0])

                with m.If(self.grant_in == 1):
                    m.d.comb += decoder.instr_in.eq(self.data_in)
                    m.d.comb += decoder.en_in.eq(1)

                    # free bus
                    m.d.sync += self.req_out.eq(0)

                    # save instruction increment for INSTR_REQ
                    m.d.sync += self.instr_size.eq(decoder.instr_size)

                    with m.Switch(decoder.instr_out):
                        with m.Case(DI.mask):

                            with m.If(decoder.mode_out == InstrMode.OFFSET):
                                m.d.sync += self.stage.eq(Stage.OFFSET_REQ)

                            with m.Elif(
                                (decoder.change_offhand == 1)
                                & (decoder.change_main == 1)
                            ):
                                # case of STORE
                                m.d.sync += self.stage.eq(Stage.WRITE_REQ)

                            with m.Else():
                                m.d.sync += self.stage.eq(Stage.READ_REQ)

                        with m.Case(DD.mask):
                            m.d.comb += (
                                # set up default signals for ALU
                                alu.a_in.eq(registers.ra_data_out),
                                alu.b_in.eq(registers.rb_data_out),

                                # set instruction size
                                alu.byte_in.eq(decoder.b_out),
                                registers.wa_byte_in.eq(decoder.b_out),
                                registers.wb_byte_in.eq(decoder.b_out),

                                # set carry input
                                alu.c_in.eq(self.msw.c),

                                # set up default register addresses
                                registers.ra_addr_in.eq(
                                    decoder.reg_offhand_out
                                ),
                                registers.rb_addr_in.eq(decoder.reg_main_out),
                                registers.wa_addr_in.eq(
                                    decoder.reg_offhand_out
                                ),
                                registers.wb_addr_in.eq(decoder.reg_main_out),

                                # normal route: via ALU to WB
                                registers.wb_enable_in.eq(1),
                            )
                            # commit operation
                            with m.Switch(decoder.op_code_out):
                                do_normal_dyadic(DD)

                                with m.Case(DD.op('STORE')):
                                    # write main register from RB to WA and
                                    # disable normal route
                                    m.d.comb += (
                                        registers.wa_data_in.eq(
                                            registers.rb_data_out
                                        ),
                                        registers.wa_enable_in.eq(1),
                                        registers.wb_enable_in.eq(0),
                                    )

                                with m.Case(DD.op('XCHG')):
                                    # write main register from RB to WA, while
                                    # offhand goes normal route: from RA
                                    # through ALU to WB
                                    m.d.comb += (
                                        alu.op_in.eq(ALUOp.OR),
                                        alu.b_in.eq(
                                            self.constants[ConstAddr.ZERO]
                                        ),
                                        registers.wa_data_in.eq(
                                            registers.rb_data_out
                                        ),
                                        registers.wb_data_in.eq(alu.s_out),
                                        registers.wa_enable_in.eq(1),
                                    )
                            # done, fetch next instruction
                            m.d.sync += self.stage.eq(Stage.INSTR_REQ)

                        with m.Case(MIS.mask):

                            # continue operation
                            with m.If(decoder.mode_out == InstrMode.OFFSET):
                                m.d.sync += self.stage.eq(Stage.OFFSET_REQ)

                            with m.Elif(decoder.change_main == 1):
                                m.d.sync += self.stage.eq(Stage.WRITE_REQ)

                            with m.Else():
                                m.d.sync += self.stage.eq(Stage.READ_REQ)

                        with m.Case(MIW.mask):

                            # continue operation
                            with m.If(decoder.mode_out == InstrMode.OFFSET):
                                m.d.sync += self.stage.eq(Stage.OFFSET_REQ)

                            with m.Elif(decoder.change_main == 1):
                                m.d.sync += self.stage.eq(Stage.WRITE_REQ)

                            with m.Else():
                                m.d.sync += self.stage.eq(Stage.READ_REQ)

                        with m.Case(MDS.mask):

                            m.d.comb += (
                                # set up default signals for ALU
                                alu.b_in.eq(registers.rb_data_out),

                                # set instruction size
                                alu.byte_in.eq(decoder.b_out),

                                # set carry input
                                alu.c_in.eq(self.msw.c),

                                # set up default register addresses
                                registers.rb_addr_in.eq(decoder.reg_main_out),
                                registers.wb_addr_in.eq(decoder.reg_main_out),
                                registers.wb_data_in.eq(alu.s_out),
                                registers.wb_byte_in.eq(decoder.b_out),
                                registers.wb_enable_in.eq(1),
                            )
                            # commit operation
                            with m.Switch(decoder.op_code_out):
                                with m.Case(MDS.op('ADC')):
                                    m.d.comb += (
                                        alu.a_in.eq(
                                            self.constants[ConstAddr.ZERO]
                                        ),
                                        alu.op_in.eq(ALUOp.ADD),
                                        alu.c_ctrl_in.eq(ALUCCtrl.PASS),
                                    )
                                    save_all_flags()

                                with m.Case(
                                    MDS.op('INC'), MDS.op('ADD2'),
                                    MDS.op('ADD4'),
                                ):
                                    m.d.comb += (
                                        alu.a_in.eq(self.constants[
                                            decoder.op_code_out[:2]
                                        ]),
                                        alu.op_in.eq(ALUOp.ADD),
                                        alu.c_ctrl_in.eq(ALUCCtrl.ZERO),
                                    )
                                    save_all_flags()

                                with m.Case(MDS.op('SBB')):
                                    m.d.comb += (
                                        alu.a_in.eq(
                                            self.constants[ConstAddr.ZERO]
                                        ),
                                        alu.op_in.eq(ALUOp.ADD),
                                        alu.invert_a_in.eq(1),
                                        alu.c_ctrl_in.eq(ALUCCtrl.INVERT),
                                    )
                                    save_all_flags()

                                with m.Case(
                                    MDS.op('DEC'), MDS.op('SUB2'),
                                    MDS.op('SUB4'),
                                ):
                                    m.d.comb += (
                                        alu.a_in.eq(self.constants[
                                            decoder.op_code_out[:2]
                                        ]),
                                        alu.op_in.eq(ALUOp.ADD),
                                        alu.invert_a_in.eq(1),
                                        alu.c_ctrl_in.eq(ALUCCtrl.ONE),
                                    )
                                    save_all_flags()

                            # done, fetch next instruction
                            m.d.sync += self.stage.eq(Stage.INSTR_REQ)

                        with m.Case(MDW.mask):

                            # TODO: actual work

                            # done, fetch next instruction
                            m.d.sync += self.stage.eq(Stage.INSTR_REQ)

                        with m.Case(C.mask):

                            # TODO: fetch instruction as data to figure out
                            #       its size and skip it
                            m.d.sync += self.stage.eq(Stage.INSTR_REQ)

                        # TODO: niladic

                        with m.Case(J.mask):

                            # treat offset as a signed number of words
                            m.d.comb += (
                                offset.data_in.eq(decoder.offset_out),
                                offset.en_in.eq(1),
                                registers.ra_addr_in.eq(Reg.IP),
                                alu.a_in.eq(registers.ra_data_out),
                                alu.b_in.eq(offset.data_out),
                                alu.op_in.eq(ALUOp.ADD),
                                alu.c_ctrl_in.eq(ALUCCtrl.ZERO),
                                registers.wb_addr_in.eq(Reg.IP),
                                registers.wb_data_in.eq(alu.s_out),
                                registers.wb_enable_in.eq(1),
                            )

                            # done, fetch next instruction
                            m.d.sync += self.stage.eq(Stage.INSTR_REQ)

                        with m.Default():

                            # TODO: invalid instruction - escalate or reset
                            m.d.sync += self.stage.eq(Stage.INSTR_REQ)

            with m.Case(Stage.OFFSET_REQ):

                # only indirect instruction types (DI, MIS, MIW)
                m.d.comb += (
                    registers.ra_addr_in.eq(Reg.IP),
                    alu.a_in.eq(registers.ra_data_out),
                    alu.b_in.eq(self.constants[ConstAddr.TWO]),
                    alu.op_in.eq(ALUOp.ADD),
                    alu.c_ctrl_in.eq(ALUCCtrl.ZERO),
                    addr_cnt.addr_in.eq(alu.s_out),
                    addr_cnt.en_in.eq(1),
                )
                m.d.sync += (
                    self.req_out.eq(1),
                    self.stage.eq(Stage.OFFSET_RSP),
                )
            with m.Case(Stage.OFFSET_RSP):
                # only after OFFSET_REQ
                with m.If(self.grant_in == 1):
                    # latch offset
                    m.d.comb += offset.data_in.eq(self.data_in)
                    m.d.comb += offset.en_in.eq(1)

                    # free bus
                    m.d.sync += self.req_out.eq(0)

                    # advance
                    with m.Switch(decoder.instr_out):
                        with m.Case(DI.mask):
                            with m.If(
                                (decoder.change_offhand == 1)
                                & (decoder.change_main == 0)
                            ):
                                m.d.sync += self.stage.eq(Stage.WRITE_REQ)

                            with m.Else():
                                m.d.sync += self.stage.eq(Stage.READ_REQ)

                        with m.Default():
                            with m.If(decoder.change_main == 1):
                                m.d.sync += self.stage.eq(Stage.WRITE_REQ)

                            with m.Else():
                                m.d.sync += self.stage.eq(Stage.READ_REQ)

            with m.Case(Stage.READ_REQ):

                # pass address through ALU for occasional offset
                m.d.comb += (
                    alu.op_in.eq(ALUOp.ADD),
                    alu.byte_in.eq(0),
                    alu.c_ctrl_in.eq(ALUCCtrl.ZERO),
                    alu.a_in.eq(registers.ra_data_out),
                )
                # get memory addressing register
                mem_reg = Signal(4)

                with m.If(decoder.instr_out.matches(DI.mask)):
                    m.d.comb += mem_reg.eq(decoder.reg_offhand_out)
                with m.Else():
                    m.d.comb += mem_reg.eq(decoder.reg_main_out)

                m.d.comb += registers.ra_addr_in.eq(mem_reg)

                with m.If(decoder.mode_out == InstrMode.OFFSET):
                    m.d.comb += alu.b_in.eq(offset.data_out)
                with m.Else():
                    m.d.comb += alu.b_in.eq(self.constants[ConstAddr.ZERO])

                # make pre-increment
                with m.If(decoder.mode_out == InstrMode.PREINCREMENT):
                    m.d.comb += addr_cnt.count_in.eq(1)
                    with m.If(mem_reg == Reg.IP):
                        m.d.comb += addr_cnt.byte_in.eq(0)
                    with m.Else():
                        m.d.comb += addr_cnt.byte_in.eq(decoder.b_out)

                m.d.comb += addr_cnt.addr_in.eq(alu.s_out)
                m.d.comb += addr_cnt.en_in.eq(1)
                m.d.sync += self.req_out.eq(1)

                # advance
                m.d.sync += self.stage.eq(Stage.READ_RSP)

            with m.Case(Stage.READ_RSP):

                with m.If(self.grant_in == 1):

                    with m.If((decoder.b_out == 0) & (self.addr_0 == 1)):
                        with m.If(
                            (decoder.instr_out.matches(DI.mask))
                            & (decoder.op_code_out == DI.op('XCHG'))
                        ):
                            m.d.comb += (
                                # save lower part of the main register
                                # to temporary latch…
                                data_lo.data_in.eq(registers.rb_data_out[:8]),

                                # … while overwrite it with higher byte of
                                # bus data
                                registers.wb_data_in.eq(self.data_in[8:]),

                                registers.wb_byte_in.eq(1),
                                registers.wb_enable_in.eq(1),
                            )
                        with m.Else():
                            # gather higher byte of bus data into
                            # low temporary latch
                            m.d.comb += data_lo.data_in.eq(self.data_in[8:])

                        m.d.comb += data_lo.en_in.eq(1)

                        m.d.sync += self.stage.eq(Stage.READ_HI_REQ)

                    with m.Else():
                        # advance
                        m.d.sync += self.stage.eq(Stage.INSTR_REQ)

                        with m.Switch(decoder.instr_out):
                            with m.Case(DI.mask):
                                # set up ALU
                                m.d.comb += alu.a_in.eq(self.data_in)
                                m.d.comb += alu.b_in.eq(registers.rb_data_out)
                                m.d.comb += alu.byte_in.eq(decoder.b_out)
                                m.d.comb += alu.c_in.eq(self.msw.c)

                                # set up registers
                                m.d.comb += registers.rb_addr_in.eq(
                                        decoder.reg_main_out
                                    )
                                m.d.comb += registers.wb_addr_in.eq(
                                        decoder.reg_main_out
                                    )
                                m.d.comb += registers.wb_byte_in.eq(
                                    decoder.b_out
                                )
                                m.d.comb += registers.wb_enable_in.eq(1)
                                m.d.comb += registers.wb_data_in.eq(alu.s_out)

                                # commit operation
                                with m.Switch(decoder.op_code_out):
                                    do_normal_dyadic(DI)

                                    # no STORE here, since it proceeds from
                                    # instruction fetch directly to WRITE_REQ

                                    with m.Case(DI.op('XCHG')):
                                        # ALU pass-through
                                        m.d.comb += alu.op_in.eq(ALUOp.OR)
                                        m.d.comb += alu.b_in.eq(
                                            self.constants[ConstAddr.ZERO]
                                        )

                                        # save main register to temp while
                                        # overwriting it with bus data
                                        m.d.comb += data_lo.data_in.eq(
                                            registers.rb_data_out[:8]
                                        )
                                        m.d.comb += data_lo.en_in.eq(1)
                                        m.d.comb += data_hi.data_in.eq(
                                            registers.rb_data_out[8:]
                                        )
                                        m.d.comb += data_hi.en_in.eq(1)

                                        # proceed to write
                                        m.d.sync += self.stage.eq(
                                            Stage.WRITE_REQ
                                        )

                    # free bus
                    m.d.sync += self.req_out.eq(0)

            with m.Case(Stage.READ_HI_REQ):

                # just increment address by one
                m.d.comb += addr_cnt.byte_in.eq(1)
                m.d.comb += addr_cnt.count_in.eq(1)
                m.d.sync += self.req_out.eq(1)

                # advance
                m.d.sync += self.stage.eq(Stage.READ_HI_RSP)

            with m.Case(Stage.READ_HI_RSP):

                with m.If(self.grant_in == 1):
                    # gather higher byte of data
                    m.d.comb += data_hi.data_in.eq(self.data_in[:8])

                    # advance
                    m.d.sync += self.stage.eq(Stage.INSTR_REQ)

                    # do operation
                    with m.Switch(decoder.instr_out):
                        with m.Case(DI.mask):
                            # set up ALU
                            m.d.comb += alu.a_in.eq(
                                Cat(data_lo.data_out, data_hi.data_out)
                            )
                            m.d.comb += alu.b_in.eq(registers.rb_data_out)
                            m.d.comb += alu.byte_in.eq(decoder.b_out)
                            m.d.comb += alu.c_in.eq(self.msw.c)

                            # set up registers
                            m.d.comb += registers.rb_addr_in.eq(
                                decoder.reg_main_out
                            )
                            m.d.comb += registers.wb_addr_in.eq(
                                decoder.reg_main_out
                            )
                            m.d.comb += registers.wb_byte_in.eq(
                                decoder.b_out
                            )
                            m.d.comb += registers.wb_enable_in.eq(1)
                            m.d.comb += registers.wb_data_in.eq(alu.s_out)

                            # commit operation
                            with m.Switch(decoder.op_code_out):
                                do_normal_dyadic(DI)

                                with m.Case(DI.op('XCHG')):
                                    # don't care about ALU pass-through

                                    # save main register to temp…
                                    m.d.comb += data_hi.data_in.eq(
                                        registers.rb_data_out[8:]
                                    )
                                    m.d.comb += data_hi.en_in.eq(1)

                                    # … while overwriting its higher byte
                                    # with lower byte of bus data
                                    #
                                    # there's no way to mask register's lower
                                    # byte from overwriting, so we need to
                                    # feed it to itself
                                    m.d.comb += registers.wb_data_in.eq(Cat(
                                        registers.rb_data_out[:8],
                                        self.data_in[:8],
                                    ))
                                    m.d.comb += registers.wb_byte_in.eq(0)
                                    m.d.comb += registers.wb_enable_in.eq(1)

                                    # proceed to write
                                    m.d.sync += self.stage.eq(
                                        Stage.WRITE_REQ
                                    )

                    # free bus
                    m.d.sync += self.req_out.eq(0)

            with m.Case(Stage.WRITE_REQ):

                # pass address through ALU for occasional offset
                m.d.comb += alu.op_in.eq(ALUOp.ADD)
                m.d.comb += alu.byte_in.eq(0)
                m.d.comb += alu.c_ctrl_in.eq(ALUCCtrl.ZERO)
                m.d.comb += alu.a_in.eq(registers.ra_data_out)

                # get memory addressing register
                mem_reg = Signal(4)

                with m.If(decoder.instr_out.matches(DI.mask)):
                    m.d.comb += mem_reg.eq(decoder.reg_offhand_out)
                with m.Else():
                    m.d.comb += mem_reg.eq(decoder.reg_main_out)

                m.d.comb += registers.ra_addr_in.eq(mem_reg)

                # add offset
                with m.If(decoder.mode_out == InstrMode.OFFSET):
                    m.d.comb += alu.b_in.eq(offset.data_out)
                with m.Else():
                    m.d.comb += alu.b_in.eq(self.constants[ConstAddr.ZERO])

                # make pre-decrement
                with m.If(decoder.mode_out == InstrMode.PREINCREMENT):
                    m.d.comb += addr_cnt.count_in.eq(1)
                    # TODO: ban use of IP and probably LR for indirect write
                    m.d.comb += addr_cnt.byte_in.eq(decoder.b_out)

                m.d.comb += addr_cnt.addr_in.eq(alu.s_out)
                m.d.comb += addr_cnt.en_in.eq(1)

                # set bus signals, use temporary registers for data output
                with m.If(self.addr_0 == 1):
                    # higher byte access or unaligned word access
                    m.d.sync += self.we_hi_out.eq(1)

                    # put lower byte high
                    m.d.comb += self.data_out.eq(
                        Cat(data_hi.data_out, data_lo.data_out)
                    )
                with m.Else():
                    m.d.comb += self.data_out.eq(
                        Cat(data_lo.data_out, data_hi.data_out)
                    )
                    with m.If(decoder.b_out == 1):
                        # lower byte access
                        m.d.sync += self.we_lo_out.eq(1)
                    with m.Else():
                        # word access
                        m.d.sync += self.we_lo_out.eq(1)
                        m.d.sync += self.we_hi_out.eq(1)

                m.d.sync += self.req_out.eq(1)

                # advance
                m.d.sync += self.stage.eq(Stage.WRITE_RSP)

            with m.Case(Stage.WRITE_RSP):

                with m.If((self.addr_0 == 1) & (decoder.b_out == 0)):
                    m.d.comb += self.data_out.eq(Cat(
                        data_hi.data_out, data_lo.data_out,
                    ))
                with m.Else():
                    m.d.comb += self.data_out.eq(Cat(
                        data_lo.data_out, data_hi.data_out,
                    ))

                with m.If(self.grant_in == 1):
                    # free bus
                    m.d.sync += self.we_lo_out.eq(0)
                    m.d.sync += self.we_hi_out.eq(0)
                    m.d.sync += self.req_out.eq(0)

                    # advance
                    with m.If((decoder.b_out == 0) & (self.addr_0 == 1)):
                        # unaligned word access
                        m.d.sync += self.stage.eq(Stage.WRITE_HI_REQ)
                    with m.Else():
                        m.d.sync += self.stage.eq(Stage.INSTR_REQ)

            with m.Case(Stage.WRITE_HI_REQ):

                # just increment address by one
                m.d.comb += addr_cnt.byte_in.eq(1)
                m.d.comb += addr_cnt.count_in.eq(1)
                m.d.comb += addr_cnt.en_in.eq(1)

                # set bus signals
                m.d.sync += self.req_out.eq(1)
                m.d.sync += self.we_lo_out.eq(1)

                # put higher byte low
                m.d.comb += self.data_out.eq(
                    Cat(data_hi.data_out, data_lo.data_out)
                )

                # advance
                m.d.sync += self.stage.eq(Stage.WRITE_HI_RSP)

            with m.Case(Stage.WRITE_HI_RSP):

                m.d.comb += self.data_out.eq(Cat(
                    data_hi.data_out, data_lo.data_out,
                ))

                with m.If(self.grant_in == 1):
                    # free bus
                    m.d.sync += self.we_lo_out.eq(0)
                    m.d.sync += self.we_hi_out.eq(0)
                    m.d.sync += self.req_out.eq(0)

                    # advance
                    m.d.sync += self.stage.eq(Stage.INSTR_REQ)

        m.d.comb += self.debug_out.eq(registers.r1)
        return m

    def ports(self) -> List[Signal]:
        return [
            self.addr_out, self.data_in, self.data_out,
            self.req_out, self.we_lo_out, self.we_hi_out,
            self.grant_in, self.debug_out,
        ]


if __name__ == '__main__':

    def process():
        for _ in range(len(fake_rom) * 2 + 10):
            yield

    parser = main_parser()
    args = parser.parse_args()

    sync = ClockDomain()
    m = Module()
    m.domains += sync
    m.submodules.control = control = ControlUnit()

    # fake ROM
    fake_rom = {
        0x0000: 0b0101000000100000,
        0x0002: 0b0101000000000001,
        0x0004: 0b0000010011111111,
    }

    with m.If(control.req_out == 1):

        m.d.comb += control.grant_in.eq(1)

        with m.Switch(control.addr_out):
            for addr, data in fake_rom.items():
                with m.Case(addr >> 1):
                    m.d.comb += control.data_in.eq(data)
            with m.Default():
                m.d.comb += control.data_in.eq(0x0000)

    sim = Simulator(m)
    sim.add_clock(1e-6, domain='sync')
    sim.add_sync_process(process)

    with sim.write_vcd(
        'control.vcd', 'control.gtkw', traces=control.ports() + [sync.clk],
    ):
        sim.run()
