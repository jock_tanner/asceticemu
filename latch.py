from enum import EnumMeta
from typing import List, Union

from nmigen import Elaboratable, Module, Signal
from nmigen.build import Platform


class Latch(Elaboratable):
    """
    Seems like nMigen do not support latches directly. See the discussion
    here: https://bugs.libre-soc.org/show_bug.cgi?id=276 .

    This module emulates a latch (or maybe a D-type flip-flop).
    """

    def __init__(self, width: Union[int, EnumMeta] = 1):
        self.data_in = Signal(width)
        self.en_in = Signal()
        self.data_out = Signal(width)

        self.data_int = Signal(width)

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        with m.If(self.en_in == 1):
            # remember the new data…
            m.d.sync += self.data_int.eq(self.data_in)
            # … while translating it to the output
            m.d.comb += self.data_out.eq(self.data_in)

        with m.Else():
            # just output the stored data
            m.d.comb += self.data_out.eq(self.data_int)

        return m

    def ports(self) -> List[Signal]:
        return [self.data_in, self.en_in, self.data_out]
