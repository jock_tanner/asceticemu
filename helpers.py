import os
import subprocess
from typing import List

from nmigen import Module, Signal
from nmigen.back import rtlil
from nmigen.hdl.ir import Fragment


SBY_CONFIG = '''\
[tasks]
bmc

[options]
bmc: mode bmc
depth 2
multiclock off

[engines]
smtbmc boolector

[script]
read_ilang {il_name}
prep -top top

[files]
{il_name}
'''


def run_test(source_file: str, module: Module, ports: List[Signal]) -> int:
    """
    Internal test runner.

    :param source_file: name of the file from which the module being tested
      was created,
    :param module: module being tested,
    :param ports: list of signals for evaluation,
    :returns: symbiyosys subprocess exit code.
    """
    base_dir, module_name = os.path.split(os.path.splitext(source_file)[0])
    if base_dir == '':
        base_dir = os.getcwd()
    elif base_dir[0] != os.path.sep:
        base_dir = os.path.join(os.getcwd(), base_dir)
    fragment = Fragment.get(module, 'formal')
    il_name = f'{module_name}.il'

    with open(os.path.join(base_dir, il_name), 'w') as il_file:
        il_file.write(rtlil.convert(fragment, name='top', ports=ports))

    with subprocess.Popen(
        ['sby', '-f'], cwd=base_dir, text=True,
        stdin=subprocess.PIPE, stdout=subprocess.PIPE,
    ) as sby:
        stdout, stderr = sby.communicate(SBY_CONFIG.format(il_name=il_name))
        if sby.returncode != 0:
            print(stdout)
        return sby.returncode
