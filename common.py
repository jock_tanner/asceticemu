from enum import Enum, IntEnum


__all__ = [
    'ALUCCtrl', 'ALUOp', 'Cond', 'ConstAddr', 'InstrMode', 'Reg', 'Stage',
]


class Reg(Enum):

    R0 = 0
    R1 = 1
    R2 = 2
    R3 = 3
    R4 = 4
    R5 = 5
    R6 = 6
    R7 = 7
    R8 = 8
    R9 = 9
    R10 = 10
    R11 = 11
    R12 = 12
    BP = 12
    R13 = 13
    SP = 13
    R14 = 14
    LR = 14
    R15 = 15
    IP = 15


class ALUOp(Enum):
    """ The operation performed. """
    ADD = 0
    AND = 1
    OR = 2
    XOR = 3


class ALUCCtrl(Enum):
    """ The source of carry input. """
    ZERO = 0
    ONE = 1
    PASS = 2
    INVERT = 3


class Cond(Enum):
    """ Condition codes for conditional instructions (SKIP). """
    EQ = 0  # equal
    Z = 0  # zero
    NEQ = 1  # not equal
    NZ = 1  # not zero
    C = 2  # carry
    NC = 3  # not carry
    NEG = 4  # negative
    POS = 5  # positive
    V = 6  # overflow
    NV = 7  # no overflow
    HI = 8  # unsigned greater than
    LS = 9  # unsigned less than or equal
    GTE = 10  # signed greater than or equal
    L = 11  # signed less than
    GT = 12  # signed greater than
    LE = 13  # signed less than or equal
    P = 14  # parity
    EVN = 14  # even
    NP = 15  # no parity
    ODD = 15  # odd


class ConstAddr(IntEnum):
    ZERO = 0
    ONE = 1
    TWO = 2
    FOUR = 3


class InstrMode(Enum):
    """ Indirect addressing modes. """

    # operand address in register
    INDIRECT = 0

    # operand address is a sum of register and immediate 16-bit offset
    OFFSET = 1

    # operand address in register incremented by operand size after use
    PREINCREMENT = 2

    # operand address in register decremented by operand size before use
    POSTDECREMENT = 3


class Stage(Enum):

    # mandatory read cycle (aligned)
    INSTR_REQ = 0
    INSTR_RSP = 1

    # optional read cycle (aligned)
    OFFSET_REQ = 2
    OFFSET_RSP = 3

    # optional read cycle (maybe unaligned)
    READ_REQ = 4
    READ_HI_REQ = 5
    READ_RSP = 6
    READ_HI_RSP = 7

    # optional write cycle (maybe unaligned)
    WRITE_REQ = 8
    WRITE_HI_REQ = 9
    WRITE_RSP = 10
    WRITE_HI_RSP = 11

    # next instruction cycle for SKIP (aligned)
    NEXT_INSTR_REQ = 12
    NEXT_INSTR_RSP = 13
