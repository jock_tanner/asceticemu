from nmigen import Elaboratable, Module, Signal
from nmigen.build import Platform

from common import Cond


class ConditionShaper(Elaboratable):

    def __init__(self):

        self.c_in = Signal()
        self.v_in = Signal()
        self.z_in = Signal()
        self.n_in = Signal()
        self.p_in = Signal()
        self.cond_code_in = Signal(Cond)

        self.result_out = Signal()

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        with m.Switch(self.cond_code_in):
            with m.Case(Cond.EQ):
                m.d.comb += self.result_out.eq(self.z_in == 1)

            with m.Case(Cond.NEQ):
                m.d.comb += self.result_out.eq(self.z_in == 0)

            with m.Case(Cond.C):
                m.d.comb += self.result_out.eq(self.c_in == 1)

            with m.Case(Cond.NC):
                m.d.comb += self.result_out.eq(self.c_in == 0)

            with m.Case(Cond.NEG):
                m.d.comb += self.result_out.eq(self.n_in == 1)

            with m.Case(Cond.POS):
                m.d.comb += self.result_out.eq(self.n_in == 0)

            with m.Case(Cond.V):
                m.d.comb += self.result_out.eq(self.v_in == 1)

            with m.Case(Cond.NV):
                m.d.comb += self.result_out.eq(self.v_in == 0)

            with m.Case(Cond.HI):
                m.d.comb += self.result_out.eq(
                    (self.c_in == 1) & (self.z_in == 0)
                )

            with m.Case(Cond.LS):
                m.d.comb += self.result_out.eq(
                    (self.c_in == 0) | (self.z_in == 1)
                )

            with m.Case(Cond.GTE):
                m.d.comb += self.result_out.eq(self.n_in == self.v_in)

            with m.Case(Cond.L):
                m.d.comb += self.result_out.eq(self.n_in != self.v_in)

            with m.Case(Cond.GT):
                m.d.comb += self.result_out.eq(
                    (self.z_in == 0) & (self.n_in == self.v_in)
                )

            with m.Case(Cond.LE):
                m.d.comb += self.result_out.eq(
                    (self.z_in == 1) | (self.n_in != self.v_in)
                )

            with m.Case(Cond.P):
                m.d.comb += self.result_out.eq(self.p_in == 1)

            with m.Case(Cond.NP):
                m.d.comb += self.result_out.eq(self.p_in == 0)

        return m
